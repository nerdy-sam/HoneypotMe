# HoneypotMe

     _  _                             _   __  __     
    | || |___ _ _  ___ _  _ _ __  ___| |_|  \/  |___ 
    | __ / _ \ ' \/ -_) || | '_ \/ _ \  _| |\/| / -_)
    |_||_\___/_||_\___|\_, | .__/\___/\__|_|  |_\___|
                       |__/|_|                       


Copyright (C) 2012  Fraunhofer FKIE

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Index
 
0. Requirements
1. Concept and functionality
2. Setting up HoneypotMe
3. Setting up the virtual machine

# Requirements

HoneypotMe is currently designed to run on Linux-Hosts only, since it heavily relies on iptables as well as netfilter tools. Therefor, the 

 * iptables-dev

package is required to run HoneypotMe. To build HoneypotMe, you'll also need

 * gcc
 * autotools and 
 * libtool 

If you'd like to run your own Honeypot to analyze unsolicited connection attempts, you'll also need a server running VirtualBox.

# Concept and functionality

HoneypotMe is a security tool designed to redirect unsolicited traffic from production hosts to honeypots. The honeypot is then able to analyze this traffic in detail while spoofing available services on potential victim systems. To achieve this, HoneypotMe listens for incoming connection attempts and checks, whether the targeted port is actually open. If this is not the case, HoneypotMe transparently tunnels the incoming connection to the configured Honeypot. 

    +------------+  Open port   +------------+
    |   Client   | -----------> |  Service   |
    +------------+              + - - - - - -+
                              |    Host    |
    +------------+ Closed port  + - - - - - -+ To honeypot
    |  Attacker  | -----------> | HoneypotMe | ----------->
    +------------+              +------------+

The entire setup basically looks like the following:

    +------------+                                                   +-------------+
    |   Victim   |                                                   | VirtualBox  |
    +- - - - - - +  UDP based tunnel  +----------+  VBox UDP tunnel  + - - - - - - +
    | HoneypotMe | <================> |  Server  | <===============> | Honeypot VM |
    +------------+      (IP data)     +----------+     (ETH data)    +-------------+

By using this setup, the virtual machine can process the tunneled incoming connection just like any other regular connection. Therefore, you can run any existing honeypot or other server within the VM. All services offered by the VM will automatically be visible on all connected HoneypotMe instances, provided that no real service is running on the same port.

# Setting up HoneypotMe

After installing the required packages, switch into the HoneypotMe root directory and run the following commands in order to build HoneypotMe:

    autoreconf -i
    ./configure
    make
    sudo make install

After this, you can start HoneypotMe by executing "honeypotme" and HoneypotMe Server by executing "honeypotme_server". You'll also find additional information about both tools in the according man pages.

# Setting up the virtual machine

Create any virtual machine image you'd like to use with HoneypotMe. There are no restrictions in the operating systems, the tools you might use or whatsoever. No dedicated tool is needed to be installed within your VM in order to make it work with HoneypotMe.

To process tunneled connections within your created VM, you need to activate VirtualBox UDP Tunnel networking (http://www.virtualbox.org/manual/ch06.html#network_udp_tunnel). By using the command line tools, this can by done by executing the following commands ("x" is the number of the network interface you'd like to use):

    VBoxManage modifyvm "VM 01 on host 1" --nic<x> generic
    VBoxManage modifyvm "VM 01 on host 1" --nicgenericdrv<x> UDPTunnel
    VBoxManage modifyvm "VM 01 on host 1" --nicproperty<x> dest=127.0.0.1
    VBoxManage modifyvm "VM 01 on host 1" --nicproperty<x> sport=10001
    VBoxManage modifyvm "VM 01 on host 1" --nicproperty<x> dport=10002 

The source and destination ports should match the VirtualBox tunnel ports used by HoneypotMe server. Additionally, you might set the MAC address of your VM to the HoneypotMe Server default 08:00:27:47:2b:4d. This can be done for example by invoking the following command.

    VBoxManage modifyvm "VM 01 on host 1" --macaddress<x> 080027472b4d

Last but not least, you need to assign the IP addresses of your HoneypotMe observed hosts to your virtual machine. On Linux guests, you can do this for example by using 

    ip addr add <your ip> dev eth1

This can of course be scripted in order to assign multiple IPs at once.
