/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <linux/netfilter.h>
#include <libipq.h>

#include "honeypotme.h"
#include "list.h"
#include "ports.h"
#include "security.h"

#include "../shared/utils.h"
#include "../shared/logging.h"
#include "../shared/udp.h"

#define MAX(a,b)         ((a < b) ?  (b) : (a))
#define BUFFER_SIZE 2048

unsigned int hpmeServerIp   = 0;
unsigned int hpmeServerPort = 0;
unsigned int listeningPort = 0;

struct ipq_handle* ipqHandle = NULL;
int monitorRawSocket       = -1;
int tunnelReadSocket   = -1;
int tunnelWriteSocket  = -1;
_Bool tcpOpenPortMap[65536];

connection_list* allowed_rst_connections = NULL;

void hpme(unsigned int ip, unsigned short remote_port, unsigned short local_port) {
    if (hpme_init(ip, remote_port, local_port) < 0) {
    	logger_error("Unable to initialize HoneypotMe");
    	return;
    }

	logger_info("HoneypotMe!");

	hpme_loop(monitorRawSocket, tunnelReadSocket, ipqHandle);
}

void hpme_loop() {
	fd_set socks;
	struct timeval tv;
	tv.tv_sec = 10;
	tv.tv_usec = 0;
	int max_fd = MAX(ipqHandle->fd, MAX(monitorRawSocket, tunnelReadSocket));

	while (1) {
		FD_ZERO(&socks);
		FD_SET(ipqHandle->fd, &socks);
		FD_SET(monitorRawSocket, &socks);
		FD_SET(tunnelReadSocket, &socks);

		int readsocks = select(max_fd + 1, &socks, (fd_set *) 0, (fd_set *) 0,
				&tv);

		if (readsocks < 0)
			break;
		else if (readsocks > 0)
			hpme_dispatchIncomingData(&socks);
	}
}

void hpme_dispatchIncomingData(fd_set* socks) {
    if (FD_ISSET(monitorRawSocket, socks))
        hpme_processMonitorData();
    
    if (FD_ISSET(tunnelReadSocket, socks))
        hpme_processTunnelData();
    
    if (FD_ISSET(ipqHandle->fd, socks))
        hpme_processIpqData();
}

int hpme_processMonitorData() {
    unsigned char buffer[BUFFER_SIZE];
    int data_size = udp_read(monitorRawSocket, buffer, BUFFER_SIZE);
    if (data_size > 0)
        return hpme_tunnelPacketIfPortClosed(buffer, data_size);
    return -1;
}

int hpme_processTunnelData() {
    unsigned char buffer[BUFFER_SIZE];
    int data_size = udp_read(tunnelReadSocket, buffer, BUFFER_SIZE);
    if (data_size > 0)
        return hpme_sendResponseToAttacker(buffer, data_size);
    return -1;
}

int hpme_processIpqData() {
    unsigned char buffer[BUFFER_SIZE];
    int res = ipq_read(ipqHandle, buffer, BUFFER_SIZE, 0);
    if (res > 0)
		return hpme_processIpqMessageIfPacket(buffer);
    return res;
}

int hpme_processIpqMessageIfPacket(unsigned char* buffer) {
	int res = -1;
	switch (ipq_message_type(buffer)) {
	case NLMSG_ERROR:
		logger_error("Received error message while reading ipq: %d",
				ipq_get_msgerr(buffer));
		logger_info("This may be caused by another process using libipq.");
		break;
	case IPQM_PACKET:
		res = hpme_passRstFromTunnel(buffer);
		break;
	default:
		logger_debug("Unknown message type!");
		break;
	}
	return res;
}

int hpme_tunnelPacketIfPortClosed(unsigned char* buffer, int size) {
	struct iphdr *ipHeader = (struct iphdr*) buffer;

	int dstIsLocal = isLocalAddress(ipHeader->daddr);
	if (dstIsLocal < 0)
		return -1;

	if (dstIsLocal == 0 || ipHeader->protocol != IPPROTO_TCP)
		return 0;
    
    uint16_t iphdrlen = ipHeader->ihl * 4;
    struct tcphdr *tcpHeader = (struct tcphdr*) (buffer + iphdrlen);
    
    if (!hpme_tcpIsOpen(tcpHeader))
        return udp_send(tunnelReadSocket, hpmeServerIp, htons(hpmeServerPort), buffer, size);
    
	return 0;
}

int hpme_sendResponseToAttacker(unsigned char* buffer, int len) {
	if (-1 == monitorRawSocket)
		return -1;

	struct iphdr *ipHeader = (struct iphdr*) buffer;
    
	if (ipHeader->protocol != IPPROTO_TCP) {
		logger_error("Received non-tcp packet from tunnel!");
		return 0;
	}
    
    uint16_t ipHeaderLength = ipHeader->ihl * 4;
    struct tcphdr *tcpHeader = (struct tcphdr*) (buffer + ipHeaderLength);

    if (security_isAllowed(ipHeader, tcpHeader) == 0) {
        logger_error("Forwarding of incoming packet is forbidden by security policies!");
		return 0;
    }
    
	if (tcpHeader->rst)
		list_addConnection(allowed_rst_connections, ipHeader, tcpHeader);
    
    logger_debug("Sending response to attacker");

	return udp_send(monitorRawSocket, ipHeader->daddr, tcpHeader->dest, buffer, len);
}

int hpme_tcpIsOpen(struct tcphdr* tcpHeader) {
    int isOpen;
    unsigned int port = ntohs(tcpHeader->dest);
    if (tcpHeader->syn && !tcpHeader->ack) {
        isOpen = port_tcpIsOpen(port);
        tcpOpenPortMap[port] = isOpen;
        logger_info("New connection attempt on closed port %u", port);
    } else
        isOpen = tcpOpenPortMap[port];
    return isOpen;
}

int hpme_passRstFromTunnel(unsigned char* buffer) {
    ipq_packet_msg_t *packet = ipq_get_packet(buffer);
    int verdict = NF_ACCEPT;
    
    if (hpme_isLocallyGeneratedRst(packet))
        verdict = NF_DROP;

    return ipq_set_verdict(ipqHandle, packet->packet_id, verdict, 0, NULL);
}

int hpme_isLocallyGeneratedRst(ipq_packet_msg_t *packet) {
    struct iphdr *ipHeader = (struct iphdr*) packet->payload;
    if (ipHeader->protocol != IPPROTO_TCP)
        return 0;
    
    uint16_t ipHeaderLength = ipHeader->ihl * 4;
    struct tcphdr *tcpHeader = (struct tcphdr*) (packet->payload + ipHeaderLength);
    
    return (tcpHeader->rst && (ntohl(tcpHeader->seq) == 0 || ntohl(tcpHeader->ack) == 0)
    		&& NULL == list_popConnection(allowed_rst_connections, ipHeader, tcpHeader));
}

int hpme_init(unsigned int remoteHost, unsigned short remotePort, unsigned short localPort) {
    allowed_rst_connections = list_create();

    hpmeServerIp = remoteHost;
    hpmeServerPort = remotePort;
    listeningPort = localPort;

    if (hpme_openIpq() < 0)
		return -1;

	if (hpme_openMonitor() < 0)
		return -1;

	if (hpme_openTunnel() < 0)
		return -1;

    hpme_initPortMap();

    return 1;
}
void hpme_initPortMap() {
    int i;
	for (i = 0; i < 65536; i++)
		tcpOpenPortMap[i] = 1;
}

int hpme_openIpq() {
	ipqHandle = ipq_create_handle(0, PF_INET);    
	if (!ipqHandle) {
		logger_error("Cannot create ipq handle");
		return -1;
	}
    
	int status = ipq_set_mode(ipqHandle, IPQ_COPY_PACKET, BUFFER_SIZE);
	if (status < 0) {
		logger_error("Cannot set ipq mode to IPQ_COPY_PACKET");
		return status;
	}
    
	return 1;
}

int hpme_openTunnel() {
    tunnelReadSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    tunnelWriteSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (tunnelReadSocket < 0 || tunnelWriteSocket < 0)
		return -1;

	return udp_bind(tunnelReadSocket, listeningPort);
}

int hpme_openMonitor() {
	monitorRawSocket = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
	if (monitorRawSocket < 0) {
		logger_error("Cannot create raw socket");
		return -1;
	}
    
	int one = 1;
	if (setsockopt(monitorRawSocket, IPPROTO_IP, IP_HDRINCL, &one, sizeof(one)) == -1) {
		logger_error("Cannot set raw socket options");
		return -1;
	}
    
	return 1;
}

void hpme_teardown() {
	printf("\n");
	logger_info("Shutting down...");

	hpme_closeTunnel();
	hpme_closeMonitor();
	hpme_closeIpq();

	list_delete(allowed_rst_connections);
    
    logger_close();
}

void hpme_closeTunnel() {
	if (tunnelReadSocket > -1) {
		close(tunnelReadSocket);
		tunnelReadSocket = -1;
	}
    if (tunnelWriteSocket > -1) {
		close(tunnelWriteSocket);
		tunnelWriteSocket = -1;
	}
}

void hpme_closeMonitor() {
	if (monitorRawSocket > -1) {
		close(monitorRawSocket);
		monitorRawSocket = -1;
	}
}

void hpme_closeIpq() {
	if (ipqHandle != NULL) {
		ipq_destroy_handle(ipqHandle);
		ipqHandle = NULL;
	}
}

/* vim: set ts=4 sw=4 et: */
