/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HONEYPOTME_H_
#define HONEYPOTME_H_

#include <netinet/tcp.h>
#include <sys/select.h>
#include <libipq.h>

void hpme(unsigned int ip, unsigned short target_port, unsigned short listen_port);
void hpme_loop();
void hpme_dispatchIncomingData(fd_set* socks);

int hpme_processMonitorData();
int hpme_processTunnelData();
int hpme_processIpqData();
int hpme_processIpqMessageIfPacket(unsigned char* buf);

void hpme_teardown();
void die();
int hpme_init(unsigned int ip, unsigned short target_port, unsigned short listen_port);

int hpme_openIpq();
int hpme_openTunnel();
int hpme_openMonitor();

void hpme_closeTunnel();
void hpme_closeMonitor();
void hpme_closeIpq();

void hpme_initPortMap();

int hpme_sendResponseToAttacker(unsigned char* buff, int len);

int hpme_tunnelPacketIfPortClosed(unsigned char* buffer, int size);

int hpme_tcpIsOpen(struct tcphdr* tcph);

int hpme_passRstFromTunnel(unsigned char* buf);

int hpme_isLocallyGeneratedRst(ipq_packet_msg_t *msg);

#endif
/* vim: set ts=4 sw=4 et: */
