/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "list.h"

connection_list* list_create() {
    connection_list* result = (connection_list*) malloc(sizeof(connection_list));
    result->length = 0;
    result->head = NULL;
    result->tail = NULL;
    return result;
}

void list_addConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph) {
	connection_list_element* new_element = (connection_list_element*) malloc(sizeof(connection_list_element));
    
	new_element->dip = iph->daddr;
	new_element->sip = iph->saddr;
	new_element->dport = tcph->dest;
	new_element->sport = tcph->source;
	new_element->next = NULL;
    
    list->length ++;
    
    if (list->tail != NULL)
        list->tail->next = new_element;
    if (list->head == NULL)
        list->head = new_element;
    list->tail = new_element;
}

connection_list_element* list_findConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph) {
    connection_list_element* current_element = list->head;
    for (; current_element != NULL; current_element = current_element->next)
        if (list_elementEqualsConnection(current_element, iph, tcph))
            return current_element;
    return NULL;
}

int list_hasConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph) {
    return (list_findConnection(list, iph, tcph) != NULL);
}

connection_list_element* list_popConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph) {
    connection_list_element* list_element = list_findConnection(list, iph, tcph);
    if (list_element != NULL)
        list_deleteElement(list, list_element);
    return list_element;
}

int list_elementEqualsConnection(connection_list_element* element, struct iphdr *iph, struct tcphdr *tcph) {
    return (element->sip == iph->saddr && element->dip == iph->daddr && element->sport == tcph->source && element->dport == tcph->dest);
}

void list_deleteElement(connection_list* list, connection_list_element* element_to_delete) {
    if (element_to_delete == NULL)
        return;
    
    connection_list_element* preceeding_element = list_findPrecedingConnectionElement(list, element_to_delete);
    
    if (preceeding_element == NULL && list->head != element_to_delete)
        return;
        
    if (list->head == element_to_delete)
        list->head = element_to_delete->next;
    if (list->tail == element_to_delete)
        list->tail = preceeding_element;
    if (preceeding_element != NULL)
        preceeding_element->next = element_to_delete->next;
    
    free(element_to_delete);
    list->length--;
}

connection_list_element* list_findPrecedingConnectionElement(connection_list* list, connection_list_element* element) {
    connection_list_element* current_element = list->head;
    if (element == current_element)
        return NULL;
    for (; current_element != NULL; current_element = current_element->next)
        if (current_element->next == element)
            return current_element;        
    return NULL;
}

void list_crop(connection_list* list) {
    list_crop_to(list, MAXIMUM_LIST_LENGTH);
}

void list_crop_to(connection_list* list, unsigned int length) {
    while (list->length > length)
        list_deleteElement(list, list->head);
}

void list_delete(connection_list* list) {
	connection_list_element* element = list->head;
	connection_list_element* freeMe = NULL;
	while (element != NULL) {
		freeMe = element;
		element = element->next;
		free(freeMe);
	}
	free(list);
}

/* vim: set ts=4 sw=4 et: */
