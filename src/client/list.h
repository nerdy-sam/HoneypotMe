/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIST_H_
#define LIST_H_

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <stdint.h>

#define MAXIMUM_LIST_LENGTH 100

typedef struct _connection_list_element {
	uint32_t sip;
	uint16_t sport;
	uint32_t dip;
	uint16_t dport;

	struct _connection_list_element* next;
} connection_list_element;

typedef struct _connection_list {
    unsigned int length;
    struct _connection_list_element* tail;
    struct _connection_list_element* head;
} connection_list;

connection_list* list_create();

void list_addConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph);

connection_list_element* list_findConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph);

connection_list_element* list_findPrecedingConnectionElement(connection_list* list, connection_list_element* element);

int list_hasConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph);

connection_list_element* list_popConnection(connection_list* list, struct iphdr *iph, struct tcphdr *tcph);

int list_elementEqualsConnection(connection_list_element* element, struct iphdr *iph, struct tcphdr *tcph);

void list_deleteElement(connection_list* list, connection_list_element* element);

void list_crop(connection_list* list);

void list_crop_to(connection_list* list, unsigned int length);

void list_delete(connection_list* list);

#endif
/* vim: set ts=4 sw=4 et: */
