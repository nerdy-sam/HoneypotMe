/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <arpa/inet.h>

#include "honeypotme.h"
#include "../shared/utils.h"
#include "../shared/logging.h"

int forkInBackground = 0;
int displayConsoleOutput = 0;
int level = LOGLEVEL_INFO;
char* logfileName = NULL;

void teardown() {
	hpme_teardown();
	if (removeIptablesRule() != 0)
		fprintf(stderr, "Unable to stop RST redirection.\n");
	printf("Bye!\n");
	exit(EXIT_SUCCESS);
}

void printUsage(char* file) {
	printf(" Usage: %s [ options ] <server> <port> <listen_port>\n", file);

	printf("\t-d, --daemon\t\tDaemonize HoneypotMe\n");
	printf("\t-h, --help\t\tShow this text\n");
    printf("\t-v, --verbose\t\tBe verbose when logging to file or stdout\n");
	printf("\t-f, --file=NAME\t\tUse this file for logging\n");
	printf("\t-c, --console\t\tLog on stdout (not with -d)\n");
    
    printf("\n");
	printf(" <server>       = Host running HoneypotMe server\n");
	printf(" <port>         = Port HoneypotMe server listens for tunneled data\n");
	printf(" <listen_port>  = Port HoneypotMe should listen for tunneled responses\n");
}

int parseOpts(int argc, char** argv) {
	int c;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
				{"daemon",  no_argument,       0, 'd'},
				{"verbose", no_argument,       0, 'v'},
				{"file",    required_argument, 0, 'f'},
				{"help",    no_argument,       0, 'h'},
				{"console", no_argument,       0, 'c'},
				{0,         0,                 0,  0 }
		};

		c = getopt_long(argc, argv, "chdvf:", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			break;
		case 'v':
			level = LOGLEVEL_DEBUG;
			break;
		case 'c':
			displayConsoleOutput = 1;
			break;
		case 'd':
			forkInBackground = 1;
			break;
		case 'f':
			logfileName = strdup(optarg);
			break;
		default:
			printf("?? getopt returned character code 0%o ??\n", c);
			break;
		}
	}

	return optind;
}

int main(int argc, char** argv) {
	signal(SIGINT,  &teardown);
	signal(SIGTERM, &teardown);
    
    int firstArgumentIndex = parseOpts(argc, argv);

    printBanner();
    
	if (argc - firstArgumentIndex < 3) {
		printUsage(argv[0]);
		return EXIT_FAILURE;
	} else if (getuid() != 0) {
        printf(" You need to be root to run HoneypotMe\n\n");
        return EXIT_FAILURE;
    }
    
    unsigned int target_ip = getNumericIpFromString(argv[firstArgumentIndex]);
    if (target_ip == INADDR_NONE) {
    	fprintf(stderr, "Invalid target IP or hostname.\n");
    	return EXIT_FAILURE;
    }
    
    unsigned short target_port;
    if((target_port = atoi(argv[firstArgumentIndex + 1])) == 0) {
        fprintf(stderr, "Invalid target port.\n");
        return EXIT_FAILURE;
    }

    unsigned short listen_port;
	if ((listen_port = atoi(argv[firstArgumentIndex + 2])) == 0) {
		fprintf(stderr, "Invalid listening port.\n");
		return EXIT_FAILURE;
	}
    
    if (applyIptablesRule() != 0) {
        fprintf(stderr, "Unable to redirect outgoing RST packets to userland using iptables.\n");
        printf("Is iptables on your path?\n");
        return EXIT_FAILURE;
    }
    
    printf("Forwarding unsolicited traffic to %s:%u\n", argv[firstArgumentIndex], target_port);
    printf("Listening for response messages on port %u\n\n", listen_port);
    
    if (forkInBackground > 0)
    	daemonize("/");

    logger_init(logfileName, displayConsoleOutput, level);
    hpme(target_ip, target_port, listen_port);

	if (removeIptablesRule() != 0)
		fprintf(stderr, "Unable to stop RST redirection.\n");

	return EXIT_SUCCESS;
}

/* vim: set ts=4 sw=4 et: */
