/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

#include "ports.h"

int port_tcpIsOpen(unsigned int port) {
    return port_findInFile(port, PATH_PROCNET_TCP);
}

int port_udpIsOpen(unsigned int port) {
    return port_findInFile(port, PATH_PROCNET_UDP);
}

int port_findInFile(unsigned int port, const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL)
        return PORT_FILE_NOT_FOUND;    
    int portFound = port_findInFileHandle(port, file);
    fclose(file);
    return portFound;
}

int port_findInFileHandle(unsigned int port, FILE* file) {
    char line[128];
    while ( fgets (line , 128 , file) != NULL )
        if (port_parseFromLine(line) == port)
            return PORT_NUMBER_FOUND;
    return PORT_NUMBER_NOT_FOUND;
}

unsigned int port_parseFromLine(char* line) {
    unsigned int port;
    if (sscanf (line,"%*u: %*X:%4X",&port))
        return port;
    else
        return PORT_NUMBER_NOT_FOUND;
}

/* vim: set ts=4 sw=4 et: */
