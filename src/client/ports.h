/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PORTS_H_
#define PORTS_H_

#define PATH_PROCNET_TCP "/proc/net/tcp"
#define PATH_PROCNET_UDP "/proc/net/udp"

#define PORT_NUMBER_FOUND 1
#define PORT_NUMBER_NOT_FOUND 0
#define PORT_FILE_NOT_FOUND -1

#include <stdio.h>

int port_tcpIsOpen(unsigned int port);

int port_udpIsOpen(unsigned int port);

int port_findInFile(unsigned int port, const char* filename);

int port_findInFileHandle(unsigned int port, FILE* file);

unsigned int port_parseFromLine(char* line);

#endif

/* vim: set ts=4 sw=4 et: */
