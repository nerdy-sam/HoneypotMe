/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <stdint.h>

#include "security.h"
#include "../shared/utils.h"

#define LOCALHOST 0x100007f

int security_isAllowed(struct iphdr* iph, struct tcphdr* tcph) {
    if (!security_validIpAddresses(iph))
		return 0;
    
    if (security_isConnectionAttempt(tcph))
		return 0;

    return 1;
}

int security_validIpAddresses(struct iphdr* iph) {
    return (isLocalAddress(iph->saddr) && !isLocalAddress(iph->daddr) && iph->daddr != LOCALHOST);
}

int security_isConnectionAttempt(struct tcphdr* tcph) {
    return (tcph->syn && !tcph->ack);
}

/* vim: set ts=4 sw=4 et: */
