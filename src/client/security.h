/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SECURITY_H_
#define SECURITY_H_

#include <netinet/ip.h>
#include <netinet/tcp.h>

int security_isAllowed(struct iphdr* iph, struct tcphdr* tcph);

int security_validIpAddresses(struct iphdr* iph);

int security_isConnectionAttempt(struct tcphdr* tcph);

#endif
/* vim: set ts=4 sw=4 et: */
