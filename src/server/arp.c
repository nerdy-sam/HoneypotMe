/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <netinet/ether.h>
#include <netinet/in.h>

#include "arp.h"

#define HARDWARE_ADDR_SIZE 6
#define PROTOCOL_ADDR_SIZE 4
#define SENDER_HARD_OFFSET 0
#define SENDER_PROT_OFFSET 6
#define TARGET_HARD_OFFSET 10
#define TARGET_PROT_OFFSET 16

unsigned char eth_bcast[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

void arp_transformToResponse(struct arphdr* arph, unsigned char* src_mac) {
    arph->ar_op = htons(ARPOP_REPLY);

    unsigned char senderHardware[HARDWARE_ADDR_SIZE];
    unsigned char senderProtocol[PROTOCOL_ADDR_SIZE];
    unsigned char targetProtocol[PROTOCOL_ADDR_SIZE];

    unsigned char* data = ((unsigned char*) arph) + sizeof(struct arphdr);

    memcpy(senderHardware, data + SENDER_HARD_OFFSET, HARDWARE_ADDR_SIZE);
    memcpy(senderProtocol, data + SENDER_PROT_OFFSET, PROTOCOL_ADDR_SIZE);
    memcpy(targetProtocol, data + TARGET_PROT_OFFSET, PROTOCOL_ADDR_SIZE);

    memcpy((unsigned char*) (data + SENDER_HARD_OFFSET), src_mac, HARDWARE_ADDR_SIZE);
    memcpy((unsigned char*) (data + SENDER_PROT_OFFSET), targetProtocol, PROTOCOL_ADDR_SIZE);
    memcpy((unsigned char*) (data + TARGET_HARD_OFFSET), senderHardware, HARDWARE_ADDR_SIZE);
    memcpy((unsigned char*) (data + TARGET_PROT_OFFSET), senderProtocol, PROTOCOL_ADDR_SIZE);
}
/* vim: set ts=4 sw=4 et: */
