/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/ip.h>
#include <netinet/ether.h>

#include "honeypotme_server.h"
#include "arp.h"
#include "../shared/utils.h"
#include "../shared/udp.h"

#define MAX(a,b)         ((a < b) ?  (b) : (a))

#define DEFAULT_HOST "127.0.0.1"
#define BUFSIZE 2048
#define ARP_LEN 28

unsigned char localMacAddress[6] = {0x00, 0x01, 0x02, 0xFA, 0x70, 0xAA};
unsigned char vmMacAddress[6] = {0x08, 0x00, 0x27, 0x47, 0x2b, 0x4d};

unsigned int vboxIpAddress = INADDR_NONE;

unsigned short portToHoneypotMe, portFromHoneypotMe, portToVbox, portFromVbox = 0;

int tunnel_read_fd  = -1;
int tunnel_write_fd = -1;
int vbox_read_fd    = -1;
int vbox_write_fd   = -1;

void hpmes_teardown() {
    if (tunnel_read_fd != -1) {
        close(tunnel_read_fd);
        tunnel_read_fd = -1;
    }
    if (tunnel_write_fd != -1) {
        close(tunnel_write_fd);
        tunnel_write_fd = -1;
    }
    if (vbox_read_fd != -1) {
        close(vbox_read_fd);
        vbox_read_fd = -1;
    }
    if (vbox_write_fd != -1) {
        close(vbox_write_fd);
        vbox_write_fd = -1;
    }
}

int hpmes_createSockets() {
    tunnel_read_fd  = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    tunnel_write_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    vbox_read_fd    = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    vbox_write_fd   = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if (tunnel_read_fd == -1 || tunnel_write_fd == -1 || vbox_read_fd == -1 || vbox_write_fd == -1)
        return -1;

    if (udp_bind(tunnel_read_fd, portFromHoneypotMe) < 0)
        return -1;

    if (udp_bind(vbox_read_fd, portFromVbox) < 0)
        return -1;
    
    return 1;
}

int hpmes_sendPacketToVbox(unsigned char* ipPacketData, int ipPacketLength, unsigned short protocol) {
    int ethernetPacketLength = ipPacketLength + sizeof(struct ethhdr);
    unsigned char ethernetPacketData[ethernetPacketLength];
    
    unsigned char *ethernetPayload = ethernetPacketData + sizeof(struct ethhdr);
    struct ethhdr *ethernetHeader = (struct ethhdr *) ethernetPacketData;
    ethernetHeader->h_proto = protocol;
     
    memcpy((void*) ethernetPacketData, (void*)vmMacAddress, ETH_ALEN);
    memcpy((void*) (ethernetPacketData + ETH_ALEN), (void*)localMacAddress, ETH_ALEN);
    memcpy((void*) ethernetPayload, ipPacketData, ipPacketLength);
 
    return udp_send(vbox_write_fd, vboxIpAddress, htons(portToVbox), ethernetPacketData, ethernetPacketLength);
}

int hpmes_handleTunnelData() {
    unsigned char buffer[BUFSIZE];
    int bytesRead = udp_read(tunnel_read_fd, buffer, BUFSIZE);
    if( bytesRead < 0)
        return -1;

    return hpmes_sendPacketToVbox(buffer, bytesRead, htons(ETH_P_IP));
}

int hpmes_sendArpResponse(unsigned char* ethernetPacketData, int ethernetPacketLength) {
    struct arphdr *arpHeader = (struct arphdr*) (ethernetPacketData + sizeof(struct ethhdr));
    int arpPacketLength = ethernetPacketLength - sizeof(struct ethhdr);

    if ((ntohs(arpHeader->ar_op) & ARPOP_REQUEST) && arpPacketLength == ARP_LEN) {
        arp_transformToResponse(arpHeader, localMacAddress);
        return hpmes_sendPacketToVbox((unsigned char*) arpHeader, ARP_LEN, htons(ETH_P_ARP));
    }

    return -1;
}

int hpmes_sendPacketToTunnel(unsigned char* ethernetPacketData, int ethernetPacketLength) {
    struct iphdr *ipHeader = (struct iphdr *)(ethernetPacketData + sizeof(struct ethhdr));

    if (ipHeader->protocol != IPPROTO_TCP)
        return -1;

    int ip_len = ethernetPacketLength - sizeof(struct ethhdr);

    return udp_send(tunnel_write_fd, ipHeader->saddr, htons(portToHoneypotMe), (unsigned char*) ipHeader, ip_len);
}

int hpmes_handleVboxData() {
    unsigned char buffer[BUFSIZE];
    int bytesRead = udp_read(vbox_read_fd, buffer, BUFSIZE);
    if (bytesRead < 0)
        return -1;

    struct ethhdr* ethernetHeader = (struct ethhdr*) buffer;
    int protocol = ntohs(ethernetHeader->h_proto);

    if (protocol == ETH_P_ARP)
        return hpmes_sendArpResponse(buffer, bytesRead);
    else if (protocol == ETH_P_IP)
        return hpmes_sendPacketToTunnel(buffer, bytesRead);

    return -1;
}

void hpmes_handleIncomingData(fd_set socks) {
    if (FD_ISSET(tunnel_read_fd, &socks))
        hpmes_handleTunnelData();
    if (FD_ISSET(vbox_read_fd, &socks))
        hpmes_handleVboxData();
}

void hpmes_loop() {
    fd_set socks;
	struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    int max_fd = MAX(tunnel_read_fd, vbox_read_fd);

	while (1) {
        FD_ZERO(&socks);
        FD_SET(tunnel_read_fd, &socks);
        FD_SET(vbox_read_fd, &socks);

		int readsocks = select(max_fd + 1, &socks, (fd_set *) 0, (fd_set *) 0, &tv);

		if (readsocks < 0)
            break;
		else if (readsocks > 0)          
			hpmes_handleIncomingData(socks);
    }
}

int hpmes(int toHoneypotMe, int fromHoneypotMe, int toVbox, int fromVbox) {
	if (vboxIpAddress == INADDR_NONE)
		vboxIpAddress = inet_addr(DEFAULT_HOST);

	portToHoneypotMe = toHoneypotMe;
	portFromHoneypotMe = fromHoneypotMe;
	portToVbox = toVbox;
	portFromVbox = fromVbox;

	if (hpmes_createSockets() < 0) {
		perror("Unable to create sockets");
		hpmes_teardown();
	    return -1;
	}

	hpmes_loop();

	return 1;
}
/* vim: set ts=4 sw=4 et: */
