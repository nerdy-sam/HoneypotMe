/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/select.h>
#include <net/if_arp.h>

#ifndef HONEYPOTME_SERVER_H_
#define HONEYPOTME_SERVER_H_

void hpmes_teardown();

int bindSocket(int sock_fd, unsigned short port);

int hpmes_createSockets();

int receiveDataFromFd(int fd, char* buf, int len);

int sendPacket(int socket, unsigned int ip, unsigned short port, unsigned char* buf, int len);

int hpmes_sendPacketToVbox(unsigned char* ip_data, int len, unsigned short proto);

int hpmes_handleTunnelData();

void transformToArpResponse(struct arphdr* arph);

int hpmes_sendArpResponse(unsigned char* buf, int len);

int hpmes_sendPacketToTunnel(unsigned char* buf, int len);

int hpmes_handleVboxData();

void hpmes_handleIncomingData(fd_set socks);

void hpmes_loop();

int hpmes(int, int, int, int);

#endif /* HONEYPOTME_SERVER_H_ */
/* vim: set ts=4 sw=4 et: */
