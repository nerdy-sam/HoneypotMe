/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <arpa/inet.h>

#include "honeypotme_server.h"
#include "../shared/utils.h"

char* newVmMacAddress = NULL;
char* newVmHostAddress = "127.0.0.1";
int forkInBackground = 0;

extern unsigned int vboxIpAddress;
extern unsigned char vmMacAddress[6];

void teardown() {
	hpmes_teardown();
	printf("\nBye!\n");
	exit(EXIT_SUCCESS);
}

void printUsage(char* file) {
	printf(" Usage: %s [ options ] <tunnelWritePort> <tunnelReadPort> <vboxWritePort> <vboxReadPort>\n", file);

	printf("\t-d, --daemon\t\tDaemonize HoneypotMe\n");
	printf("\t-h, --help\t\tShow this text\n");
	printf("\t-m, --mac=ADDR\t\tUse this MAC address for VirtualBox VM\n");
	printf("\t\t\t\t(Defaults to %s)\n", getMacAsString(vmMacAddress));
	printf("\t-a, --host=ADDR\t\tUse this address as VirtualBox host\n");
	printf("\t\t\t\t(Defaults to %s)\n", newVmHostAddress);

	printf("\n");
	printf(" <tunnelWritePort> = Port used to send packets to HoneypotMe\n");
	printf(" <tunnelReadPort>  = Port used to receive packets from HoneypotMe\n");
	printf(" <vboxWritePort>   = Port used to send packets to VirtualBox VM.\n");
	printf(" <vboxReadPort>    = Port used receive packets from VirtualBox VM.\n");
}

int parseOpts(int argc, char** argv) {
	int c;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
				{"daemon",  no_argument,       0, 'd'},
				{"help",    no_argument,       0, 'h'},
				{"host",    required_argument, 0, 'a'},
				{"mac",     required_argument, 0, 'm'},
				{0,         0,                 0,  0 }
		};

		c = getopt_long(argc, argv, "a:dhm:", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			break;
		case 'a':
			newVmHostAddress = strdup(optarg);
			break;
		case 'd':
			forkInBackground = 1;
			break;
		case 'm':
			newVmMacAddress = strdup(optarg);
			break;
		default:
			printf("?? getopt returned character code 0%o ??\n", c);
			break;
		}
	}

	return optind;
}

int setMacAddress(char* mac) {
	if (NULL == mac)
		return 0;
	short unsigned int addr[6];
	int res = sscanf(mac, "%2hx:%2hx:%2hx:%2hx:%2hx:%2hx", &(addr[0]), &(addr[1]), &(addr[2]), &(addr[3]), &(addr[4]), &(addr[5]));
	if (res < 6)
		return -1;
	int i;
	for (i = 0; i < 6; i++)
		vmMacAddress[i] = addr[i];
	return 1;
}

int main (int argc, char** argv) {
	signal(SIGINT,  &teardown);
	signal(SIGTERM, &teardown);

	printBanner();

    int firstArgumentIndex = parseOpts(argc, argv);

    if (argc - firstArgumentIndex < 4) {
    	printUsage(argv[0]);
        return EXIT_FAILURE;
    }

    int toHoneypotMe, fromHoneypotMe, toVbox, fromVbox = -1;

    if ((toHoneypotMe = atoi(argv[firstArgumentIndex])) == 0){
    	fprintf(stderr, "Invalid tunnel write port\n");
        return EXIT_FAILURE;
    }
    
    if ((fromHoneypotMe = atoi(argv[firstArgumentIndex +1])) == 0){
    	fprintf(stderr, "Invalid tunnel read port\n");
        return EXIT_FAILURE;
    }
    
    if ((toVbox = atoi(argv[firstArgumentIndex + 2])) == 0){
    	fprintf(stderr, "Invalid VBox write port\n");
        return EXIT_FAILURE;
    }
    
    if ((fromVbox = atoi(argv[firstArgumentIndex +3])) == 0){
    	fprintf(stderr, "Invalid VBox read port\n");
        return EXIT_FAILURE;
    }

    vboxIpAddress = getNumericIpFromString(newVmHostAddress);
    if (vboxIpAddress == INADDR_NONE) {
    	fprintf(stderr, "Invalid host name or IP address\n");
    	return EXIT_FAILURE;
    }

    if (setMacAddress(newVmMacAddress) < 0) {
    	fprintf(stderr, "Invalid MAC address\n");
    	return EXIT_FAILURE;
    }

    printf("Starting HoneypotMe server.\n\n");
    printf("Expecting VirtualBox VM to run on %s\n", newVmHostAddress);
    printf("Expecting MAC address of VM to be %s\n\n", getMacAsString(vmMacAddress));
	printf("HoneypotMe ---> HoneypotMe Server:%u ---> VirtualBox VM:%u\n", fromHoneypotMe, toVbox);
	printf("HoneypotMe:%u <--- HoneypotMe Server:%u <--- VirtualBox VM\n\n", toHoneypotMe, fromVbox);

	if (forkInBackground > 0)
		daemonize("/");

    hpmes(toHoneypotMe, fromHoneypotMe, toVbox, fromVbox);

    perror("Exiting");

    return EXIT_SUCCESS;
}
/* vim: set ts=4 sw=4 et: */
