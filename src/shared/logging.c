/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "logging.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#define TIMESTAMP_LENGTH 20

FILE* logfile = NULL;

_Bool log_to_stdout = 0;

unsigned int loglevel = 0;

_Bool logger_init(const char* fname, const int console,
		const unsigned int level) {

	if (fname != NULL) {
		logfile = fopen(fname, "a+");
		if (logfile == NULL)
			return 0;
	}

	loglevel = level;
	log_to_stdout = console;

	return 1;
}

void logger_write(const char* fmt, FILE* console, const char* label,
		const int len, va_list ap1, va_list ap2) {
	struct tm *local;
	time_t t = time(NULL);
	local = localtime(&t);

	/* format + timestamp + label_length + braces + space + newline */
	int l = strlen(fmt) + TIMESTAMP_LENGTH + len + 2 + 1 + 2;

	char new_fmt[l];
	bzero(new_fmt, l);
	sprintf(new_fmt, "[%d/%02d/%02d %02d:%02d:%02d %s] %s\n", local->tm_year
			+ 1900, local->tm_mon, local->tm_mday, local->tm_hour,
			local->tm_min, local->tm_sec, label, fmt);

	if (console != NULL)
		vfprintf(console, new_fmt, ap1);

	if (NULL != logfile) {
		vfprintf(logfile, new_fmt, ap2);
		fflush(logfile);
	}

}

void _logger_debug(const char* fmt, ...) {
	if (LOGLEVEL_DEBUG < loglevel)
		return;

	va_list ap1, ap2;
	va_start(ap1,fmt);
	va_start(ap2,fmt);

	logger_write(fmt, (log_to_stdout) ? stdout : NULL, "DEBUG", 5, ap1, ap2);

	va_end(ap1);
	va_end(ap2);
}

void logger_info(const char* fmt, ...) {
	if (LOGLEVEL_INFO < loglevel)
		return;

	va_list ap1, ap2;
	va_start(ap1,fmt);
	va_start(ap2,fmt);

	logger_write(fmt, (log_to_stdout) ? stdout : NULL, "INFO", 4, ap1, ap2);

	va_end(ap1);
	va_end(ap2);
}

void logger_warn(const char* fmt, ...) {
	if (LOGLEVEL_WARN < loglevel)
		return;

	va_list ap1, ap2;
	va_start(ap1,fmt);
	va_start(ap2,fmt);

	logger_write(fmt, (log_to_stdout) ? stdout : NULL, "WARN", 4, ap1, ap2);

	va_end(ap1);
	va_end(ap2);
}

void _logger_error(const char* fmt, ...) {
	if (LOGLEVEL_ERROR < loglevel)
		return;

	va_list ap1, ap2;
	va_start(ap1,fmt);
	va_start(ap2,fmt);

	logger_write(fmt, (log_to_stdout) ? stderr : NULL, "ERROR", 5, ap1, ap2);

	va_end(ap1);
	va_end(ap2);
}

void logger_fatal(const char* fmt, ...) {
	if (LOGLEVEL_FATAL < loglevel)
		return;

	va_list ap1, ap2;
	va_start(ap1,fmt);
	va_start(ap2,fmt);

	logger_write(fmt, (log_to_stdout) ? stderr : NULL, "FATAL", 5, ap1, ap2);

	va_end(ap1);
	va_end(ap2);
}

void logger_close() {
	if (NULL != logfile) {
		fclose(logfile);
        logfile = NULL;
    }
}
/* vim: set ts=4 sw=4 et: */
