/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef LOGGING_H_
#define LOGGING_H_

#include <stdarg.h>
#include <stdio.h>

#define LOGLEVEL_DEBUG 0
#define LOGLEVEL_INFO 1
#define LOGLEVEL_WARN 2
#define LOGLEVEL_ERROR 3
#define LOGLEVEL_FATAL 4

#define logger_debug(fmt, s...) _logger_debug(fmt " (%s:%u)", ## s, __FILE__, __LINE__)

#define logger_error(fmt, s...) _logger_error(fmt " (%m)", ## s)

_Bool logger_init(const char* fname, const int console, const unsigned int level);

void logger_write(const char* fmt, FILE* console, const char* label, const int len, va_list ap1, va_list ap2);

void _logger_debug(const char* msg, ...);

void logger_info(const char* msg, ...);

void logger_warn(const char* msg, ...);

void _logger_error(const char* msg, ...);

void logger_fatal(const char* msg, ...);

void logger_close();

#endif /* LOGGING_H_ */
/* vim: set ts=4 sw=4 et: */
