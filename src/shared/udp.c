/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "udp.h"

int udp_send(int socket, unsigned int ip, unsigned short port, unsigned char* buff, int len) {
    struct sockaddr_in daddr;
    memset(&daddr, 0, sizeof(daddr));
	daddr.sin_family = AF_INET;
	daddr.sin_addr.s_addr = ip;
	daddr.sin_port = port;

	if (sendto(socket, buff, len, 0, (struct sockaddr *) &daddr, sizeof(daddr)) != len)
		return 0;
	return 1;
}

int udp_bind(int sock_fd, unsigned short port) {
    struct sockaddr_in saddr;

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(port);

    int err = bind(sock_fd, (struct sockaddr *) &saddr, sizeof(saddr));
    if (err < 0)
        close(sock_fd);
    return err;
}

int udp_read(int fd, unsigned char* buf, int len) {
    struct sockaddr saddr;
    socklen_t saddr_size = sizeof saddr;
    bzero(buf, len);

    return recvfrom(fd, buf, len, 0, &saddr, &saddr_size);
}
/* vim: set ts=4 sw=4 et: */
