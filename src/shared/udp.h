/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UDP_H_
#define UDP_H_

int udp_send(int socket, unsigned int ip, unsigned short port, unsigned char* buff, int len);

int udp_bind(int sock_fd, unsigned short port);

int udp_read(int fd, unsigned char* buf, int len);

#endif /* UDP_H_ */
/* vim: set ts=4 sw=4 et: */
