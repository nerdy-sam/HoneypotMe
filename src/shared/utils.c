/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <fcntl.h>
#include <syslog.h>
#include <sys/stat.h>

#include "utils.h"

#define IPTABLES_RULE "OUTPUT -p tcp --tcp-flags RST RST -j QUEUE"

void daemonize(char* dir) {
	pid_t pid, sid;
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0) {
		printf("Forked daemon process with id %u\n", pid);
		exit(EXIT_SUCCESS);
	}

	umask(0);
	sid = setsid();
	if (sid < 0)
		exit(EXIT_FAILURE);

	if ((chdir(dir)) < 0)
		exit(EXIT_FAILURE);

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
}

unsigned int getNumericIpFromString(char* str) {
	int ip;
	if((ip = inet_addr(str)) != INADDR_NONE)
		return ip;
	struct hostent* host = gethostbyname(str);
	if (host != NULL)
		return ((struct in_addr *)host->h_addr_list[0])->s_addr;
	return INADDR_NONE;
}

char* getMacAsString(unsigned char mac[6]) {
	static char result[18];
	bzero(result, 18);
	sprintf(result, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	return result;
}

uint16_t getLocalIpAddresses(uint32_t** local_addr_v) {
	struct ifaddrs * ifAddrStruct = NULL;
	struct sockaddr_in * temp_addr = NULL;

	getifaddrs(&ifAddrStruct);
	uint32_t local_addr_c = 0;

	while (ifAddrStruct != NULL ) {
		if (ifAddrStruct->ifa_addr != NULL && ifAddrStruct->ifa_addr->sa_family == AF_INET) {
			temp_addr = (struct sockaddr_in *) ifAddrStruct->ifa_addr;

			local_addr_c++;
			(*local_addr_v) = realloc(*local_addr_v, local_addr_c * sizeof(uint32_t));
			(*local_addr_v)[local_addr_c - 1] = temp_addr->sin_addr.s_addr;
		}

		ifAddrStruct = ifAddrStruct->ifa_next;
	}
	return local_addr_c;
}

int isLocalAddress(uint32_t addr) {
	static uint32_t* local_addr_v = NULL;
	static uint16_t local_addr_c = 0;

	if (local_addr_c == 0) {
		local_addr_c = getLocalIpAddresses(&local_addr_v);

		if (local_addr_c == 0)
			return -1;
	}

	uint16_t i;
	for (i = 0; i < local_addr_c; i++)
		if (local_addr_v[i] == addr)
			return 1;

	return 0;
}

/* return nicely formatted string */
char* getPacketInfo(unsigned char* in_buff) {
	static char out_buff[512];
	bzero(out_buff, 512);

	struct iphdr *iph = (struct iphdr*) in_buff;
	uint16_t iphdrlen = iph->ihl * 4;

	struct in_addr src;
	src.s_addr = iph->saddr;

	struct in_addr dst;
	dst.s_addr = iph->daddr;

	char * src_addr = strdup(inet_ntoa(src));
	char * dst_addr = strdup(inet_ntoa(dst));

	if (iph->protocol == IPPROTO_TCP) {
		struct tcphdr *tcph = (struct tcphdr*) (in_buff + iphdrlen);

		sprintf(out_buff, "%s:%u -> %s:%u ", src_addr, ntohs(tcph->source),
				dst_addr, ntohs(tcph->dest));

		if (tcph->syn)
			strcat(out_buff, "S");

		if (tcph->fin)
			strcat(out_buff, "F");

		if (tcph->ack)
			strcat(out_buff, "A");

		if (tcph->rst)
			strcat(out_buff, "R");

		if (tcph->psh)
			strcat(out_buff, "P");
	} else {
		sprintf(out_buff, "%s -> %s", src_addr, dst_addr);
	}

	free(src_addr);
	free(dst_addr);

	return out_buff;
}

void printBanner() {
    printf("  _  _                             _   __  __     \n");
    printf(" | || |___ _ _  ___ _  _ _ __  ___| |_|  \\/  |___ \n");
    printf(" | __ / _ \\ ' \\/ -_) || | '_ \\/ _ \\  _| |\\/| / -_)\n");
    printf(" |_||_\\___/_||_\\___|\\_, | .__/\\___/\\__|_|  |_\\___|\n");
    printf("                    |__/|_|                       \n");
    printf("\n HoneypotMe Copyright (c) 2012  Fraunhofer FKIE\n");
    printf(" This program comes with ABSOLUTELY NO WARRANTY\n\n");
}

int applyIptablesRule() {
    return system("iptables -A "IPTABLES_RULE);
}

int removeIptablesRule() {
    return system("iptables -D "IPTABLES_RULE);
}
/* vim: set ts=4 sw=4 et: */
