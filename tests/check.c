/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <unistd.h>
#include <check.h>
#include <stdlib.h>

#include "check_honeypotme.h"
#include "check_ports.h"
#include "check_list.h"

Suite *honeypotme_unit_suite (void) {
    Suite *s = suite_create ("HoneypotMe Unit");

    suite_add_tcase (s, check_ports_case());
    suite_add_tcase (s, check_list_case());

    return s;
}

Suite *honeypotme_acceptance_suite (void) {
    Suite *s = suite_create ("HoneypotMe Acceptance");
    
    suite_add_tcase (s, check_honeypotme_case());

    return s;
}

int main(void) {
    printf("Starting HoneypoMe test suite\n");
    
    SRunner *sr = srunner_create (honeypotme_unit_suite());

    if (getuid() == 0) {
    	printf("Adding acceptance suite.\n");
    	srunner_add_suite (sr, honeypotme_acceptance_suite());
    } else {
        printf("\n");
        printf("+----------------------------------------------------------+\n");
        printf("| Skipping acceptance tests - Re-run with root privileges! |\n");
        printf("+----------------------------------------------------------+\n\n");
    }

    srunner_run_all (sr, CK_NORMAL);
	int number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
/* vim: set ts=4 sw=4 et: */
