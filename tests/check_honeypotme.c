/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>
#include <netpacket/packet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/ether.h>
#include <pcap.h>

#include "check_honeypotme.h"

#include "../src/server/honeypotme_server.h"
#include "../src/client/honeypotme.h"
#include "../src/client/ports.h"
#include "../src/client/list.h"
#include "../src/shared/udp.h"

#define SLEEP_TIME 100000
#define BUFSIZE 2048
#define LOCALHOST  "127.0.0.1"
#define REMOTEHOST "127.0.0.2"
#define TEST_IF    "lo"

pid_t honeypotMePid = 0;
pid_t honeypotMeServerPid = 0;
pid_t echoReplyServerPid = 0;

unsigned int honeypotMeToServer = 0;
unsigned int serverToHoneypotMe = 0;

unsigned int serverToVbox = 0;
unsigned int vboxToServer = 0;

int last_free_port = 1024;

START_TEST (check_honeypotMeForwarding)
{
	struct sockaddr_in saddr;
	unsigned char buf[BUFSIZE];

	check_initPorts();
	int testPort = check_nextFreePort();

	check_startHoneypotMe(honeypotMeToServer, serverToHoneypotMe);

    int serverFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, serverFd, honeypotMeToServer) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendSynPacketToPort(testPort);
		_exit(EXIT_SUCCESS);
	}

	int nread = udp_read(serverFd, buf, BUFSIZE);

	check_validateMessageToServer(nread, buf);

	check_stopProcesses();
}
END_TEST

START_TEST (check_honeypotMeReply)
{
	check_initPorts();

	check_startHoneypotMe(honeypotMeToServer, serverToHoneypotMe);

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendServerResponse(serverToHoneypotMe);
		_exit(EXIT_SUCCESS);
	}

	check_validateMessageToAttacker();

	check_stopProcesses();
}
END_TEST

START_TEST (check_serverForwarding)
{
	struct sockaddr_in saddr;
	unsigned char buf[BUFSIZE];

	check_initPorts();
	check_startHoneypotMeServer(serverToHoneypotMe, honeypotMeToServer, serverToVbox, vboxToServer);

    int serverFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, serverFd, serverToVbox) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendTunneledSynPacketToPort(honeypotMeToServer);
		_exit(EXIT_SUCCESS);
	}

	int nread = udp_read(serverFd, buf, BUFSIZE);
	fail_unless(nread >=  sizeof(struct iphdr) + sizeof(struct tcphdr),
				"Received invalid data");

	check_validateMessageToVbox(nread, buf);

	check_stopProcesses();
}
END_TEST

START_TEST (check_serverReply)
{
	struct sockaddr_in saddr;
	unsigned char buf[BUFSIZE];

	check_initPorts();
	check_startHoneypotMeServer(serverToHoneypotMe, honeypotMeToServer, serverToVbox, vboxToServer);

	int serverFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, serverFd, serverToHoneypotMe) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendVboxResponse(vboxToServer);
		_exit(EXIT_SUCCESS);
	}

	int nread = udp_read(serverFd, buf, BUFSIZE);
	fail_unless(nread >=  sizeof(struct iphdr) + sizeof(struct tcphdr),
			"Received invalid data");

	check_validateMessageToHoneypotMe(nread, buf);

	check_stopProcesses();
}
END_TEST

START_TEST (check_serverArpResponse)
{
	struct sockaddr_in saddr;
	unsigned char buf[BUFSIZE];

    check_startProcesses();

    int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, vboxFd, serverToVbox) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendArpRequest(vboxToServer);
		_exit(EXIT_SUCCESS);
	}

	printf("Waiting for arp response. Should arrive within 1 second...\n");
	int nread = udp_read(vboxFd, buf, BUFSIZE);

	check_validateArpResponse(nread, buf);

	check_stopProcesses();
}
END_TEST

START_TEST (check_completeForwarding)
{
	struct sockaddr_in saddr;
	unsigned char buf[BUFSIZE];

	check_startProcesses();

    int testPort = check_nextFreePort();

    int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, vboxFd, serverToVbox) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendSynPacketToPort(testPort);
		_exit(EXIT_SUCCESS);
	}

	printf("Waiting for tunneled data. Should arrive within 1 second...\n");
	int nread = udp_read(vboxFd, buf, BUFSIZE);

	check_validateMessageToVbox(nread, buf);

	check_stopProcesses();
}
END_TEST

START_TEST (check_completeReply)
{
	struct sockaddr_in saddr;

    check_startProcesses();

    int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(check_bindListeningPort(&saddr, vboxFd, serverToVbox) >= 0,
			"Cannot bind port %m");

	pid_t pid = fork();
	fail_unless(pid >= 0,
			"Cannot fork %m");

	if (pid == 0) {
		usleep(SLEEP_TIME);
		check_sendVboxResponse(vboxToServer);
		_exit(EXIT_SUCCESS);
	}

	check_validateMessageToAttacker();

	check_stopProcesses();
}
END_TEST

void check_validateMessageToHoneypotMe(int nread, unsigned char buf[BUFSIZE]) {
	fail_unless(nread >= sizeof(struct iphdr) + sizeof(struct tcphdr),
				"Received invalid data");
	struct iphdr* iph = (struct iphdr*) buf;
	fail_unless(iph->protocol == IPPROTO_TCP,
			"Incoming data should be valid TCP packet, but was");
	uint16_t iphdrlen = iph->ihl * 4;
	struct tcphdr* tcph = (struct tcphdr*) (buf + iphdrlen);
	fail_unless(!tcph->syn && tcph->ack,
			"Incoming packet should be TCP ACK packet");
}

void check_validateMessageToAttacker() {
	pcap_t* handle;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct bpf_program fp;
	char filter_exp[] =
			"tcp dst port 8888 and tcp src port 9999 and src net "LOCALHOST" and dst net "REMOTEHOST;
	bpf_u_int32 mask;
	bpf_u_int32 net;
	struct pcap_pkthdr header;
	const u_char* packet;
	fail_unless(pcap_lookupnet(TEST_IF, &net, &mask, errbuf) != -1,
			"Couldn't get netmask for device %s: %s\n", TEST_IF, errbuf);
	handle = pcap_open_live(TEST_IF, BUFSIZ, 1, 1000, errbuf);
	fail_unless(handle != NULL,
			"Couldn't open device %s: %s\n", TEST_IF, errbuf);
	fail_unless(pcap_compile(handle, &fp, filter_exp, 0, net) != -1,
			"Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
	fail_unless(pcap_setfilter(handle, &fp) != -1,
			"Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
	while ((packet = pcap_next(handle, &header)) == NULL )
		;
	pcap_close(handle);
}

void check_validateArpResponse(int nread, unsigned char buf[BUFSIZE]) {
	fail_unless(nread >=  sizeof(struct ethhdr) + sizeof(struct arphdr) + 20,
				"Received invalid data");
	unsigned char* arp_data = buf + sizeof(struct ethhdr);
	struct arphdr* arph = (struct arphdr*) arp_data;
	fail_unless((ntohs(arph->ar_op) & ARPOP_REPLY) != 0,
			"Unexpected arp operation");
	fail_unless(arph->ar_hln == 6, "Unexpected hardware length");
	fail_unless(arph->ar_pln == 4, "Unexpected protocol length");
	fail_unless(arph->ar_hrd == htons(ETH_P_ARP), "Unexpected hardware");
	fail_unless(arph->ar_pro == htons(ETH_P_IP), "Unexpected protocol");
	unsigned char* payload = arp_data + sizeof(struct arphdr);
	unsigned char* response_mac = payload + 10;
	int all_zero = 1;
	int n;
	for (n = 0; n < 6; n++)
		if (response_mac[n] != 0) {
			all_zero = 0;
			break;
		}
	fail_unless(all_zero == 0, "Got empty mac address");
}

void check_validateMessageToServer(int nread, unsigned char buf[BUFSIZE]) {
	fail_unless(nread >= sizeof(struct iphdr) + sizeof(struct tcphdr),
			"Received invalid data");
	struct iphdr* iph = (struct iphdr*) buf;
	fail_unless(iph->protocol == IPPROTO_TCP,
			"Incoming data should be valid TCP packet, but was");
	uint16_t iphdrlen = iph->ihl * 4;
	struct tcphdr* tcph = (struct tcphdr*) (buf + iphdrlen);
	fail_unless(tcph->syn && !tcph->ack,
			"Incoming packet should be TCP SYN packet");
}

void check_validateMessageToVbox(int nread, unsigned char buf[BUFSIZE]) {
	fail_unless(
			nread >= sizeof(struct ethhdr) + sizeof(struct iphdr) + sizeof(struct tcphdr),
			"Received invalid data");
	unsigned char* ip_data = buf + sizeof(struct ethhdr);
	struct iphdr* iph = (struct iphdr*) ip_data;
	fail_unless(iph->protocol == IPPROTO_TCP,
			"Incoming data should be valid TCP packet, but was");
	uint16_t iphdrlen = iph->ihl * 4;
	struct tcphdr* tcph = (struct tcphdr*) (ip_data + iphdrlen);
	fail_unless(tcph->syn && !tcph->ack,
			"Incoming packet should be TCP SYN packet");
}

void check_craftEthPacket(struct ethhdr* ethh) {
	bzero(ethh, sizeof(struct ethhdr));
	ethh->h_proto = htons(ETH_P_IP);
}

void check_craftIpPacket(struct iphdr* iph) {
	bzero(iph, sizeof(struct iphdr));
	iph->daddr = inet_addr(REMOTEHOST);
	iph->saddr = inet_addr(LOCALHOST);
	iph->version = 4;
	iph->protocol = IPPROTO_TCP;
	iph->ihl = sizeof(struct iphdr) / 4;
	iph->ttl = 16;
	iph->id = 0;
}

void check_craftTcpPacket(struct tcphdr* tcph) {
	bzero(tcph, sizeof(struct tcphdr));
	tcph->source = htons(9999);
	tcph->dest = htons(8888);
	tcph->ack = 1;
	tcph->window = htons(128);
	tcph->doff = sizeof(struct tcphdr) / 4;
}

int check_sendArpRequest(int toServer) {
	int len = sizeof(struct ethhdr) + sizeof(struct arphdr) + 20;

	unsigned char buf[len];
	bzero(buf, len);

	struct ethhdr* ethh = (struct ethhdr*) buf;
	ethh->h_proto = htons(ETH_P_ARP);

	struct arphdr* arph = (struct arphdr*) (buf + sizeof(struct ethhdr));

	arph->ar_hln = 6;
	arph->ar_pln = 4;
	arph->ar_op = htons(ARPOP_REQUEST);
	arph->ar_hrd = htons(ETH_P_ARP);
	arph->ar_pro = htons(ETH_P_IP);

	unsigned int local_ip = inet_addr(LOCALHOST);
	unsigned int remote_ip = inet_addr(REMOTEHOST);

	unsigned char* payload = buf + sizeof(struct ethhdr) + sizeof(struct arphdr);

	memcpy(payload, (unsigned char*) &remote_ip, 4);
	memcpy(payload + 4, "aaaaaa", 6);
	memcpy(payload + 16, (unsigned char*) &local_ip, 4);

	int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	printf("Sending arp request to server %u\n", toServer);
	int res = udp_send(vboxFd, local_ip, htons(toServer), buf, len);
	return res;
}

int check_sendVboxResponse(int toServer) {
	struct ethhdr ethh;
	check_craftEthPacket(&ethh);

	struct iphdr iph;
	check_craftIpPacket(&iph);

	struct tcphdr tcph;
	check_craftTcpPacket(&tcph);

	int bufferSize = sizeof(struct ethhdr) + sizeof(struct iphdr) + sizeof(struct tcphdr);
	unsigned char buffer[bufferSize];
	bzero(buffer, bufferSize);

	memcpy(buffer, &ethh, sizeof(struct ethhdr));
	memcpy(buffer + sizeof(struct ethhdr), &iph, sizeof(struct iphdr));
	memcpy(buffer + sizeof(struct ethhdr) + sizeof(struct iphdr), &tcph, sizeof(struct tcphdr));

	int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	printf("Sending vbox response to server: %u\n", toServer);
	return udp_send(vboxFd, inet_addr(LOCALHOST), htons(toServer), buffer, bufferSize);
}

int check_sendServerResponse(int toHoneypotme) {
	struct iphdr iph;
	check_craftIpPacket(&iph);

	struct tcphdr tcph;
	check_craftTcpPacket(&tcph);

	int bufferSize = sizeof(struct iphdr) + sizeof(struct tcphdr);
	unsigned char buffer[bufferSize];
	bzero(buffer, bufferSize);

	memcpy(buffer, &iph, sizeof(struct iphdr));
	memcpy(buffer + sizeof(struct iphdr), &tcph, sizeof(struct tcphdr));

	int vboxFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	printf("Sending vbox response to server: %u\n", toHoneypotme);
	return udp_send(vboxFd, inet_addr(LOCALHOST), htons(toHoneypotme), buffer, bufferSize);
}

void check_sendSynPacketToPort(int port) {
	struct sockaddr_in addr;
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		perror("ERROR opening socket");
	bzero(&addr, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(LOCALHOST);

	if (-1 == connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)))
	    close(sockfd);
}

void check_sendTunneledSynPacketToPort (int port) {
	unsigned int length = sizeof(struct iphdr) + sizeof(struct tcphdr);
	unsigned char data[length];
	struct iphdr* ipHeader = (struct iphdr*) data;
	struct tcphdr* tcpHeader = (struct tcphdr*) (data + sizeof(struct iphdr));

	check_craftIpPacket(ipHeader);
	check_craftTcpPacket(tcpHeader);

	tcpHeader->ack = 0;
	tcpHeader->syn = 1;

	int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	fail_unless(udp_send(s, inet_addr(LOCALHOST), htons(port), data, length),
			"Unable to send tunneled packet");
}

void check_initPorts() {
	honeypotMeToServer = check_nextFreePort();
	serverToHoneypotMe = check_nextFreePort();
	serverToVbox = check_nextFreePort();
	vboxToServer = check_nextFreePort();
}

int check_startProcesses() {
	printf("Starting HoneypotMe processes...\n");

	check_initPorts();
	check_startHoneypotMe(honeypotMeToServer, serverToHoneypotMe);
	check_startHoneypotMeServer(serverToHoneypotMe, honeypotMeToServer, serverToVbox, vboxToServer);

	return 1;
}

void check_stopProcesses() {
	printf("Stopping HoneypotMe processes...\n");

	if (honeypotMePid > 0) {
		kill(honeypotMePid, SIGINT);
		honeypotMePid = 0;
	}
	if (honeypotMeServerPid > 0) {
		kill(honeypotMeServerPid, SIGINT);
		honeypotMeServerPid = 0;
	}
	if (echoReplyServerPid > 0) {
		kill(echoReplyServerPid, SIGINT);
		echoReplyServerPid = 0;
	}

	printf("Waiting for processes to exit... \n");
	sleep(1);
	printf("Done.\n");
}

int check_bindListeningPort (struct sockaddr_in* saddr, int fd, int port) {
	memset(saddr, 0, sizeof(saddr));
	saddr->sin_family = AF_INET;
	saddr->sin_addr.s_addr = INADDR_ANY;
	saddr->sin_port = htons(port);

	return bind(fd, (struct sockaddr *) saddr, sizeof(struct sockaddr));
}

int check_nextFreePort() {
    int i;
    for (i = last_free_port + 1; i < 64000; i++)
        if (port_udpIsOpen(i) <= 0) {
        	last_free_port = i;
            return i;
        }
    return -1;
}

int check_startHoneypotMe(int honeypotMeToServer, int serverToHoneypotMe) {
	pid_t pid = fork();
	if (pid < 0)
		return -1;
	else if (pid == 0) {
		hpme(inet_addr(LOCALHOST), honeypotMeToServer, serverToHoneypotMe);
		perror("HoneypotMe exited");
		_exit(EXIT_SUCCESS);
	} else
		honeypotMePid = pid;
    return 1;
}

int check_startHoneypotMeServer(int toHoneypotMe, int fromHoneypotMe, int toVbox, int fromVbox) {
	pid_t pid = fork();
	if (pid < 0)
		return -1;
	else if (pid == 0) {
		hpmes(toHoneypotMe, fromHoneypotMe, toVbox, fromVbox);
		perror("HoneypotMe server exited");
		_exit(EXIT_SUCCESS);
	} else
		honeypotMeServerPid = pid;
	return 1;
}

TCase *check_honeypotme_case (void) {
    TCase *tc = tcase_create ("HoneypotMe acceptance suite");
    tcase_add_test (tc, check_honeypotMeForwarding);
    tcase_add_test (tc, check_honeypotMeReply);
    tcase_add_test (tc, check_serverForwarding);
    tcase_add_test (tc, check_serverReply);
    tcase_add_test (tc, check_serverArpResponse);
    tcase_add_test (tc, check_completeForwarding);
    tcase_add_test (tc, check_completeReply);
    return tc;
}
/* vim: set ts=4 sw=4 et: */
