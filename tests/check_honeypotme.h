/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHECK_HONEYPOTME_H_
#define CHECK_HONEYPOTME_H_

#include <netinet/in.h>
#include <check.h>

TCase* check_honeypotme_case();

int check_nextFreePort();

int check_startHoneypotMe(int honeypotMeToServer, int serverToHoneypotMe);

void check_validateMessageToVbox(int nread, unsigned char *);
void check_validateMessageToHoneypotMe(int nread, unsigned char *);
void check_validateMessageToServer(int nread, unsigned char* buf);
void check_validateMessageToAttacker();
void check_validateArpResponse(int nread, unsigned char* buf);

int check_startProcesses();
void check_stopProcesses();

void check_initPorts();
int check_startHoneypotMeServer();
int check_startEchoReplyServer();

void check_sendTunneledSynPacketToPort();
void check_sendSynPacketToPort(int port);
int check_sendServerResponse(int toHoneypotme);
int check_sendVboxResponse(int toServer);
int check_sendArpRequest(int toServer);

int check_bindListeningPort (struct sockaddr_in* saddr, int fd, int port) ;

#endif
/* vim: set ts=4 sw=4 et: */
