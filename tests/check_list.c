/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <stdio.h>

#include "check_list.h"
#include "../src/client/list.h"

START_TEST (check_emptyList)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    iph.saddr = 1;
    iph.daddr = 1;
    tcph.source = 1;
    tcph.dest = 1;
    
    fail_unless(list_hasConnection(list, &iph, &tcph) <= 0, "Packet should not be on stack");
}
END_TEST

START_TEST (check_singleElement)
{
    struct iphdr iph;
	struct tcphdr tcph;
    int i = 1;
    connection_list* list = list_create();

    iph.saddr = i;
    iph.daddr = i;
    tcph.source = i;
    tcph.dest = i;
    
    list_addConnection(list, &iph, &tcph);
    connection_list_element* element = list_findConnection(list, &iph, &tcph);
    
    fail_unless(element != NULL, "Packet not found on stack");
    fail_unless(element == list->head, "Invalid list head");
    fail_unless(element == list->tail, "Invalid list tail");
}
END_TEST

START_TEST (check_deleteSingleElement)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    iph.saddr = 1;
    iph.daddr = 1;
    tcph.source = 1;
    tcph.dest = 1;
    
    list_addConnection(list, &iph, &tcph);
    fail_unless(list_hasConnection(list, &iph, &tcph), "Packet should be on stack");
    list_deleteElement(list, list->head);
    fail_unless(list_hasConnection(list, &iph, &tcph) <= 0, "Packet should not be on stack");
    fail_unless(list->head == NULL, "Invalid list head");
    fail_unless(list->tail == NULL, "Invalid list tail");
    fail_unless(list->length == 0, "List should be empty now");
}
END_TEST

START_TEST (check_deleteFirstElement)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    int i;
    for (i = 0; i < 10; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        list_addConnection(list, &iph, &tcph);
    }
    
    list_deleteElement(list, list->head);
    
    for (i = 1; i < 10; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        fail_unless(list_hasConnection(list, &iph, &tcph), "Packet should be in list");
    }
    
    iph.saddr = 0;
    iph.daddr = 0;
    tcph.source = 0;
    tcph.dest = 0;
    fail_unless(list_hasConnection(list, &iph, &tcph) <= 0, "Packet should not be in list");
}
END_TEST

START_TEST (check_deleteLastElement)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    int i;
    for (i = 0; i < 10; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        list_addConnection(list, &iph, &tcph);
    }
    
    connection_list_element* last_element;
    for (last_element = list->head; last_element && last_element->next; last_element = last_element->next)
        ;
    list_deleteElement(list, last_element);
    
    for (i = 0; i < 9; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        fail_unless(list_hasConnection(list, &iph, &tcph), "Packet should be in list");
    }
    
    iph.saddr = 9;
    iph.daddr = 9;
    tcph.source = 9;
    tcph.dest = 9;
    fail_unless(list_hasConnection(list, &iph, &tcph) <= 0, "Packet should not be in list");
}
END_TEST

START_TEST (check_deleteMiddleElement)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    int i;
    for (i = 0; i < 10; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        list_addConnection(list, &iph, &tcph);
    }
    
    connection_list_element* middle_element;
    for (middle_element = list->head; middle_element && middle_element->next; middle_element = middle_element->next)
        if (middle_element->dip == 4)
            break;
    list_deleteElement(list, middle_element);
    
    for (i = 0; i < 10; i++) {
        if (i == 4)
            continue;
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        fail_unless(list_hasConnection(list, &iph, &tcph), "Packet should be in list");
    }
    
    iph.saddr = 4;
    iph.daddr = 4;
    tcph.source = 4;
    tcph.dest = 4;
    fail_unless(list_hasConnection(list, &iph, &tcph) <= 0, "Packet should not be in list");
}
END_TEST

START_TEST (check_crop)
{
    struct iphdr iph;
	struct tcphdr tcph;
    connection_list* list = list_create();
    
    int i;
    for (i = 0; i < 10; i++) {
        iph.saddr = i;
        iph.daddr = i;
        tcph.source = i;
        tcph.dest = i;
        
        list_addConnection(list, &iph, &tcph);
    }
    
    list_crop_to(list, 5);
    fail_unless(list->length == 5, "Invalid list length");
    fail_unless(list->head->sip == 5, "Invalid first list element");
}
END_TEST

TCase* check_list_case(void) {
    TCase *tc = tcase_create ("List");
    tcase_add_test (tc, check_emptyList);
    tcase_add_test (tc, check_singleElement);
    tcase_add_test (tc, check_deleteSingleElement);
    tcase_add_test (tc, check_deleteFirstElement);
    tcase_add_test (tc, check_deleteLastElement);
    tcase_add_test (tc, check_deleteMiddleElement);
    tcase_add_test (tc, check_crop);
    return tc;
}
/* vim: set ts=4 sw=4 et: */
