/*
 HoneypotMe - Redirect attacks from production systems
 Copyright (C) 2012  Fraunhofer FKIE
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <check.h>

#include "../src/client/ports.h"

#define PROC_NET_TCP_LINE "0: 0100007F:2BCB 00000000:0000 0A 00000000:00000000 00:00000000 00000000   114        0 8207 1 00000000 100 0 0 10 -1 "

#define PROC_NET_TCP_LINE_INVALID "  sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode"

START_TEST (check_parsePortFromLine)
{
    uint16_t port = port_parseFromLine(PROC_NET_TCP_LINE);
    fail_unless ( port == 11211, "Port could not be parsed from line" );
}
END_TEST

START_TEST (check_parsePortFromInvalidLine)
{
    uint16_t port = port_parseFromLine(PROC_NET_TCP_LINE_INVALID);
    fail_unless ( port == PORT_NUMBER_NOT_FOUND, "Parsed bogus port from invalid line" );
}
END_TEST

START_TEST (check_parsePortFromFile)
{
    int status = port_findInFile(11211, "tcp");
    fail_unless ( status > 0, "Cannot find port in file" );
}
END_TEST

START_TEST (check_parsePortFromInvalidFile)
{
    int status = port_findInFile(11211, "noSuchFile");
    fail_unless ( status == PORT_FILE_NOT_FOUND, "Cannot find port in file" );
}
END_TEST

TCase* check_ports_case(void) {
    TCase *tc = tcase_create ("Ports");
    tcase_add_test (tc, check_parsePortFromLine);
    tcase_add_test (tc, check_parsePortFromInvalidLine);
    tcase_add_test (tc, check_parsePortFromFile);
    tcase_add_test (tc, check_parsePortFromInvalidFile);
    return tc;
}
/* vim: set ts=4 sw=4 et: */
